# view articles

It's a project for view news.

## Technologies used
* [Dart](https://dart.dev)
* [Flutter](https://flutter.dev)
* [Dio](https://pub.dev/packages/dio)
* [BLOC](https://bloclibrary.dev)

## Api
* [The New York Times Api](https://developer.nytimes.com/apis)

This project is a starting point for a Flutter application.

## run app
#### 1. Getting the key 
Register to [New York Times Api](https://developer.nytimes.com/apis) and make api key
#### 2. Run
You can run app in console
```
flutter run --dart-define NYTApiKey=apiKey
```
#### OR
You can sad ide configuration and run 'main.dart'. 
Edit Configuration --> Additional run args
```
--dart-define="NYTApiKey=apiKey"
```