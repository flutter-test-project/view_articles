enum ApiSettings {
  newsApi(
      urlApi: 'https://newsapi.org/v2',
      keyApi: String.fromEnvironment('newsApiKey'),
      urlHome: '/top-headlines'),
  newYorkTimes(
      urlApi: 'https://api.nytimes.com/svc/topstories/v2',
      keyApi: String.fromEnvironment('NYTApiKey'),
      urlHome: '/home.json');

  const ApiSettings({
    required this.urlApi,
    required this.keyApi,
    required this.urlHome,
  });

  final String urlApi;
  final String keyApi;
  final String urlHome;
}
