import 'package:intl/intl.dart';

String shotData(String data) {
  String formattedDate =
      DateFormat('MM.dd.yy H:M').format(DateTime.parse(data));
  return formattedDate;
}

String shotTime(String data) {
  String formattedDate =
  DateFormat('H:M').format(DateTime.parse(data));
  return formattedDate;
}
