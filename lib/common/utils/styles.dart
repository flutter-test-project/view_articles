import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//Color
const Color backgroundColor = Color.fromRGBO(244, 244, 244, 1);

const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
const Color grayColor = Color.fromRGBO(109, 109, 118, 1.0);
const Color blueColor = Color.fromRGBO(32, 74, 181, 1.0);
const Color grayPurpleColor = Color.fromRGBO(90, 84, 117, 1.0);
const Color brownPurpleColor = Color.fromRGBO(33, 29, 49, 1.0);
const Color blackColor = Color.fromRGBO(0, 0, 0, 1.0);

const Color redColor = Color.fromRGBO(244, 67, 54, 1.0);
const Color greenColor = Color.fromRGBO(63, 145, 109, 1.0);
const Color yellowColor = Color.fromRGBO(187, 129, 53, 1.0);
const Color lightGrayColor = Color.fromRGBO(153, 153, 153, 1.0);

//Style
TextStyle appBarTextStyle = GoogleFonts.robotoSlab(
  textStyle: const TextStyle(
      color: blackColor,
      fontWeight: FontWeight.bold,
      fontSize: 18,
      height: 24 / 18),
);

TextStyle headerStyle = GoogleFonts.crimsonText(
  textStyle: const TextStyle(
    color: blackColor,
    fontWeight: FontWeight.bold,
    fontSize: 20,
  ),
);

const headingStyle = TextStyle(
  color: Colors.black,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);

TextStyle normalStyle = GoogleFonts.domine(
  textStyle: const TextStyle(
    color: brownPurpleColor,
    fontSize: 14,
  ),
);
