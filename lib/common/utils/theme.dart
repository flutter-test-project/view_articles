import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:view_articles/common/utils/styles.dart';

final theme = ThemeData(
  primaryColor: Colors.grey,
  scaffoldBackgroundColor: backgroundColor,
  colorScheme: const ColorScheme.light(
    primary: Colors.red,
    secondary: Colors.white,
  ),
  buttonTheme: const ButtonThemeData(textTheme: ButtonTextTheme.primary),
  visualDensity: VisualDensity.adaptivePlatformDensity,
  inputDecorationTheme: const InputDecorationTheme(
    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 6),
  ),
  appBarTheme: AppBarTheme(
    elevation: 0,
    systemOverlayStyle:
        SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent),
    iconTheme: const IconThemeData(color: Colors.black54),
    backgroundColor: backgroundColor,
    titleTextStyle: appBarTextStyle,
  ),
);
