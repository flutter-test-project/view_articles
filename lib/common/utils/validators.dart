String? notEmptyTextFieldValidator(String? text) {
  if (text == null || text.isEmpty) {
    return 'Обязательное поле';
  } else {
    return null;
  }
}