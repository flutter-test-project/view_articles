import 'dart:developer';
import 'dart:math' hide log;

import 'package:dio/dio.dart';
import 'package:view_articles/domain/models/article.dart';

import '../../../../common/app_config.dart';
import '../../../../domain/models/multimedia.dart';
import '../../../../domain/repository/article_repository.dart';

class ArticleRepositoryNewsApiHttp implements ArticleRepository {
  @override
  Future<List<Article>> getArticleList() async {
    try {
      final response = await Dio().get(
        '${ApiSettings.newsApi.urlApi}${ApiSettings.newsApi.urlHome}',
        queryParameters: {
          'country': 'ru',
          'category': 'general',
        },
        options: Options(
          headers: {'X-Api-Key': ApiSettings.newsApi.keyApi},
        ),
      );
      if (response.statusCode == 200) {
        return (response.data['articles'] as List)
            .map(
              (e) => Article(
                id: Random().nextInt(10000),
                text: e['description'] ?? '',
                title: e['title'] ?? '',
                data: e['publishedAt'] ?? '',
                author: e['author'] ?? '',
                url: e['url'] ?? '',
                multimedias: [
                  Multimedia(
                    url: e['urlToImage'] ?? '',
                    type: 'image',
                  ),
                ],
              ),
            )
            .toList();
      }

      return [];
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }
}
