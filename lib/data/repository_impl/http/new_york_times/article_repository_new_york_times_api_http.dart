import 'dart:developer';
import 'dart:math' hide log;
import 'package:dio/dio.dart';
import 'package:view_articles/domain/models/article.dart';

import '../../../../common/app_config.dart';
import '../../../../domain/models/multimedia.dart';
import '../../../../domain/repository/article_repository.dart';

class ArticleRepositoryNewYorkTimesApiHttp implements ArticleRepository {
  @override
  Future<List<Article>> getArticleList() async {
    try {
      final response = await Dio().get(
        '${ApiSettings.newYorkTimes.urlApi}${ApiSettings.newYorkTimes.urlHome}',
        queryParameters: {
          'api-key': ApiSettings.newYorkTimes.keyApi,
        },
      );

      if (response.statusCode == 200) {
        return (response.data['results'] as List)
            .map(
              (e) => Article(
                id: Random().nextInt(10000),
                text: e['abstract'] ?? '',
                title: e['title'] ?? '',
                data: e['updated_date'] ?? '',
                author: e['author'] ?? '',
                url: e['url'] ?? '',
                multimedias: (e['multimedia'] as List)
                    .map(
                      (multimedia) => Multimedia(
                        url: multimedia['url'],
                        width: multimedia['width'],
                        height: multimedia['height'],
                        type: multimedia['type'],
                      ),
                    )
                    .toList(),
              ),
            )
            .toList();
      }
      return [];
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }
}
