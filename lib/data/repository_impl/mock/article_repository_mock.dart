import 'dart:convert';
import 'dart:developer';

import 'package:flutter/services.dart';
import 'package:view_articles/domain/models/article.dart';
import 'package:view_articles/domain/repository/article_repository.dart';

class ArticleRepositoryMock implements ArticleRepository {
  @override
  Future<List<Article>> getArticleList() async {
    try {
      final String response =
          await rootBundle.loadString('assets/json/articles.json');
      final List data = await json.decode(response);
      await Future.delayed(const Duration(seconds: 2), () {});
      return data
          .map(
            (e) => Article.fromJson(e),
          )
          .toList();
    } catch (error) {
      log(error.toString());
      rethrow;
    }
  }
}
