import 'dart:convert';
import 'dart:developer';

import 'package:flutter/services.dart';
import 'package:view_articles/domain/models/user.dart';

import '../../../domain/repository/user_repository.dart';

class UserRepositoryMock implements UserRepository {
  @override
  Future<User?> signIn(
      {required String login, required String password}) async {
    try {
      final String response =
          await rootBundle.loadString('assets/json/users.json');
      final List data = await json.decode(response);
      await Future.delayed(const Duration(seconds: 2), () {});
      for (var element in data) {
        if (element['login'] == login && element['password'] == password) {
          return User.fromJson(element);
        }
      }

      return null;
    } catch (error) {
      log('Пользователь не найден');
      rethrow;
    }
  }
}
