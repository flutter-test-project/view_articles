import 'package:get_it/get_it.dart';
import 'data/repository_impl/http/new_york_times/article_repository_new_york_times_api_http.dart';
import 'data/repository_impl/mock/user_repository_mock.dart';
import 'domain/repository/article_repository.dart';
import 'domain/repository/user_repository.dart';
import 'domain/usecases/get_articles_usecases.dart';

final getIt = GetIt.instance;

void init() {
  //getIt.registerSingleton<ArticleRepository>(ArticleRepositoryNewsApiHttp());
  getIt.registerSingleton<ArticleRepository>(
      ArticleRepositoryNewYorkTimesApiHttp());
  getIt.registerSingleton<UserRepository>(UserRepositoryMock());

  //useCases
  getIt.registerSingleton(GetArticlesUseCases(articleRepository: getIt()));
}
