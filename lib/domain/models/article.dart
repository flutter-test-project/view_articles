import 'package:equatable/equatable.dart';

import 'multimedia.dart';

class Article extends Equatable {
  final int id;
  final String title;
  final String text;
  final String data;
  final String author;
  final String url;
  final List<Multimedia> multimedias;

  const Article({
    required this.id,
    required this.title,
    required this.text,
    required this.data,
    this.author = '',
    this.url = '',
    this.multimedias = const [],
  });

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      id: json['id'],
      title: json['title'],
      text: json['text'],
      data: json['data'],
    );
  }

  @override
  List<Object?> get props => [
        id,
        title,
        text,
        data,
        author,
        url,
        multimedias,
      ];
}
