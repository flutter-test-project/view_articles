import 'package:equatable/equatable.dart';

class Multimedia extends Equatable {
  final String url;
  final int height;
  final int width;
  final String type;

  const Multimedia({
    required this.url,
    this.height = 200,
    this.width = 1200,
    required this.type,
  });

  @override
  List<Object?> get props => [
        url,
        height,
        width,
        type,
      ];
}
