import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String accessToken;
  final String fullName;

  const User({
    required this.accessToken,
    required this.fullName,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      accessToken: json['token'],
      fullName: json['full_name'],
    );
  }

  @override
  List<Object?> get props => [accessToken, fullName];
}
