import '../models/user.dart';

abstract class UserRepository {
  Future<User?> signIn({
    required String login,
    required String password,
  });
}
