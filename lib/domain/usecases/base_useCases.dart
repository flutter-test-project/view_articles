abstract class BaseUseCases<Result, Params> {
  Result call(Params params);
}

class NoParams {}
