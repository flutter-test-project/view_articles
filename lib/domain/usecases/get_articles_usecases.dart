import 'package:view_articles/domain/usecases/base_useCases.dart';

import '../models/article.dart';
import '../repository/article_repository.dart';

class GetArticlesUseCases
    extends BaseUseCases<Future<List<Article>>, NoParams> {
  final ArticleRepository _articleRepository;

  GetArticlesUseCases({required ArticleRepository articleRepository})
      : _articleRepository = articleRepository;

  @override
  Future<List<Article>> call(NoParams params) async {
    final List<Article> result = await _articleRepository.getArticleList();
    return result;
  }
}
