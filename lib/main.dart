import 'package:flutter/material.dart';
import 'package:view_articles/dependency_register.dart';
import 'package:view_articles/presentation/home/home_screen.dart';

import 'common/utils/theme.dart';

void main() {
  init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'View Articles',
      theme: theme,
      home: const HomeScreen(),
    );
  }
}
