import 'package:flutter/material.dart';

import '../../common/utils/styles.dart';
import '../../domain/models/article.dart';
import '../ui/app_bar/image_app_bar.dart';

class ArticleScreen extends StatelessWidget {
  final Article article;

  const ArticleScreen(this.article, {super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ImageAppBar(imageMultimedia: article.multimedias.first),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        child: ListView(
          children: [
            Text(
              article.title,
              style: appBarTextStyle.copyWith(color: Colors.red),
            ),
            const SizedBox(height: 8),
            Text(
              'От: ${article.data}',
              style: normalStyle,
            ),
            const SizedBox(height: 8),
            Text(
              article.text,
              style: normalStyle,
            ),
          ],
        ),
      ),
    );
  }
}
