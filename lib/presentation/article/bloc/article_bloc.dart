import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:view_articles/domain/usecases/base_useCases.dart';
import 'package:view_articles/domain/usecases/get_articles_usecases.dart';

import '../../../domain/models/article.dart';
import '../../../domain/repository/article_repository.dart';

part 'article_event.dart';

part 'article_state.dart';

class ArticleBloc extends Bloc<ArticleEvent, ArticleState> {
  //final ArticleRepository _articleRepository;
  final GetArticlesUseCases _getArticlesUseCase;

  ArticleBloc(this._getArticlesUseCase) : super(ArticleInit()) {
    on<ArticleFetchedEvent>(_onLoading);
    on<ArticleReloadEvent>(_onReload);
  }

  Future<void> _onLoading(
    ArticleFetchedEvent event,
    Emitter<ArticleState> emit,
  ) async {
    emit(ArticleLoadingState());
    try {
      final List<Article> articles = await _getArticlesUseCase.call(NoParams());

      emit(ArticleLoadedState(articles));
    } catch (error) {
      emit(ArticleErrorState(error.toString()));
    }
  }

  Future<void> _onReload(
    ArticleReloadEvent event,
    Emitter<ArticleState> emit,
  ) async {
    final oldState = state;
    if (oldState is ArticleLoadedState) {
      emit(ArticleLoadingState());
      try {
        final List<Article> articles =
            await _getArticlesUseCase.call(NoParams());

        emit(ArticleLoadedState(articles));
      } catch (error) {
        emit(ArticleErrorState(error.toString()));
      }
    }
  }
}
