part of 'article_bloc.dart';

@immutable
abstract class ArticleEvent extends Equatable {
  const ArticleEvent();

  @override
  List<Object?> get props => [];
}

class ArticleFetchedEvent extends ArticleEvent {}

class ArticleReloadEvent extends ArticleEvent {}
