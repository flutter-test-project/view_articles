part of 'article_bloc.dart';

@immutable
abstract class ArticleState extends Equatable {
  const ArticleState();

  @override
  List<Object> get props => [];
}

class ArticleInit extends ArticleState {}

class ArticleLoadingState extends ArticleState {}

class ArticleLoadedState extends ArticleState {
  final List<Article> articles;

  const ArticleLoadedState(this.articles);
}

class ArticleErrorState extends ArticleState {
  final String errorText;

  const ArticleErrorState(this.errorText);
}
