import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import '../../common/utils/data.dart';
import '../../common/utils/styles.dart';
import '../article/bloc/article_bloc.dart';
import '../../domain/models/article.dart';
import '../article/article_screen.dart';
import '../ui/app_bar/main_app_bar.dart';
import '../ui/custom_progress_indicator.dart';
import '../ui/dialogs/error_dialog.dart';
import 'widgets/news_panel_shot_image.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => ArticleBloc(GetIt.instance.call()),
      child: _HomePage(),
    );
  }
}

class _HomePage extends StatefulWidget {
  @override
  State<_HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  @override
  void initState() {
    _init(context);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        text: 'Articles',
        isShowBackButton: false,
      ),
      body: RefreshIndicator(
        onRefresh: () => _refresh(context),
        child: BlocConsumer<ArticleBloc, ArticleState>(
          listener: (context, state) {
            if (state is ArticleErrorState) {
              showErrorDialog(
                context: context,
                errorText: state.errorText,
                text: 'Ошибка',
              );
            }
          },
          builder: (context, state) {
            if (state is ArticleLoadingState) {
              return const CustomProgressIndicator();
            } else if (state is ArticleLoadedState) {
              return ListView(
                children: state.articles.map((e) => _TopicPanel(e)).toList(),
              );
            } else {
              return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}

class _TopicPanel extends StatelessWidget {
  final Article article;

  const _TopicPanel(this.article);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ArticleScreen(article),
        ),
      ),
      child: Container(
        //padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        margin: const EdgeInsets.only(bottom: 8, left: 12, right: 12, top: 4),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
          color: Colors.white,
        ),

        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // const CustomImagePanel(
              //   //article.urlToImage,
              //   'https://docs.flutter.dev/assets/images/dash/dash-fainting.gif',
              //   width: 100,
              //   height: 100,
              // ),
              Flexible(
                child: article.multimedias.isNotEmpty
                    ? NewsPanelShotImage(
                        imagePath: article.multimedias.first.url,
                      )
                    : Container(),
              ),

              const SizedBox(width: 8),
              Flexible(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.topRight,
                        child: Text(
                          shotTime(article.data),
                        ),
                      ),
                      Text(
                        article.title,
                        style: headerStyle,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SizedBox(height: 8),
                      Text(
                        article.text,
                        style: normalStyle,
                        textAlign: TextAlign.start,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(width: 8),
            ],
          ),
        ),
      ),
    );
  }
}

// class _ExitButton extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return IconButton(
//       padding: const EdgeInsets.symmetric(horizontal: 8),
//       onPressed: () {
//         Navigator.pushReplacement(
//           context,
//           MaterialPageRoute(
//             builder: (BuildContext context) => const LoginScreen(),
//           ),
//         );
//       },
//       icon: const Icon(Icons.exit_to_app),
//     );
//   }
// }

Future<void> _init(BuildContext context) async {
  BlocProvider.of<ArticleBloc>(context).add(ArticleFetchedEvent());
}

Future<void> _refresh(BuildContext context) async {
  BlocProvider.of<ArticleBloc>(context).add(ArticleReloadEvent());
}
