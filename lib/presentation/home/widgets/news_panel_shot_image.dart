import 'package:flutter/material.dart';

class NewsPanelShotImage extends StatelessWidget {
  final String imagePath;

  const NewsPanelShotImage({
    super.key,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(12),
        bottomLeft: Radius.circular(12),
      ),
      child: Image.network(
        imagePath,
        cacheHeight: 1000,
        //width: double.infinity,
        fit: BoxFit.fitHeight,
        errorBuilder: (context, url, error) {
          return Container(
              padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 42),
              color: Colors.white,
              child: const Center(
                child: Text('Изображение не было найдено'),
              ));
        },
        frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
          return child;
        },
        loadingBuilder: (BuildContext context, Widget child,
            ImageChunkEvent? loadingProgress) {
          if (loadingProgress == null) {
            return child;
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
