import 'package:flutter/material.dart';

import '../../../domain/models/multimedia.dart';
import '../buttons/custom_back_button.dart';

class ImageAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Multimedia imageMultimedia;

  const ImageAppBar({
    super.key,
    required this.imageMultimedia,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: const CustomBackButton(),
      centerTitle: true,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(imageMultimedia.url),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  @override
  //Size get preferredSize => const Size.fromHeight(kToolbarHeight);
  Size get preferredSize => const Size.fromHeight(220);
}
