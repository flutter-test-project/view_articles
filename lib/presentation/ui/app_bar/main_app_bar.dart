import 'package:flutter/material.dart';
import 'package:view_articles/common/utils/styles.dart';

import '../buttons/custom_back_button.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String text;
  final bool isShowBackButton;
  const MainAppBar({
    super.key,
    required this.text,
    this.isShowBackButton = true,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: isShowBackButton ? const CustomBackButton() : null,
      title: Text(
        text.toUpperCase(),
        style: appBarTextStyle,
      ),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
