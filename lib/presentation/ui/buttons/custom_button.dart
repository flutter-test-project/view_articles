import 'package:flutter/material.dart';

import '../../../common/utils/styles.dart';

class CustomButton extends StatelessWidget {
  final String text;
   final VoidCallback? onPressed;

  const CustomButton({super.key, required this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.bottomCenter,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        color: Colors.red[300],
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: const BorderSide(color: Colors.black),
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 16.0,
        ),
        onPressed: onPressed,
        child: Text(
          text,
          style: normalStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
