import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../common/utils/styles.dart';

void showErrorDialog({
  required BuildContext context,
  required String text,
  required String errorText,
  IconData icon = CupertinoIcons.clear_circled,
}) {
  showDialog<String>(
      context: context,
      builder: (BuildContext context) => ErrorDialog(
            text: text,
            errorText: errorText,
            icon: icon,
          ));
}

class ErrorDialog extends StatefulWidget {
  final String text;
  final String errorText;
  final IconData icon;

  const ErrorDialog({
    Key? key,
    required this.text,
    required this.errorText,
    required this.icon,
  }) : super(key: key);

  @override
  State<ErrorDialog> createState() => _ErrorDialogState();
}

class _ErrorDialogState extends State<ErrorDialog> {
  var _isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 8),
          Icon(
            widget.icon,
            size: 60,
            color: Colors.grey[400],
          ),
          const SizedBox(height: 8),
          Text(widget.text, style: headingStyle),
          ExpansionPanelList(
            expandedHeaderPadding: const EdgeInsets.all(0),
            elevation: 0,
            expansionCallback: (index, bool isExpanded) {
              setState(() {
                _isExpanded = !_isExpanded;
              });
            },
            children: [
              ExpansionPanel(
                backgroundColor: Colors.transparent,
                canTapOnHeader: true,
                isExpanded: _isExpanded,
                headerBuilder: (BuildContext _, bool isExpanded) {
                  return  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Текст ошибки:',
                        style: normalStyle,
                      ),
                    ],
                  );
                },
                body: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  child: Text(
                    widget.errorText,
                    style: normalStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            ],
          )
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'OK'),
          child: const Text('OK'),
        ),
      ],
    );
  }
}
