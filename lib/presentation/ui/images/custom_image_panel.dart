import 'package:flutter/material.dart';

class CustomImagePanel extends StatelessWidget {
  final String imageUrl;
  final double? width;
  final double? height;

  const CustomImagePanel(
    this.imageUrl, {
    super.key,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      color: Colors.green,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        //onTap: () => openNetworkImageDialog(context, imageUrl),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            imageUrl,
            errorBuilder: (context, url, error) {
              return Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 42),
                  color: Colors.white,
                  child:
                      const Center(child: Text('Изображение не было найдено')));
            },
          ),
        ),
      ),
    );
  }
}
